# Summary
#### 这是一个简单的聊天界面, 模拟了Client和服务器通讯的过程. <br> 界面主要有两个隔离的Room组成. 可以通过Server互通消息. 每个Room都可以上线/下线. Server有缓存消息的功能.
#### 公司有个面试作业,让自己做一个聊天界面. 然后经常看别人提交的作品,惨不忍睹. 有的人代码更是从github/gitlab上down下来直接修改提交的. 本demo仅供参考.拒绝抄袭.
# How to use?
### 本demo基于[nodejs](https://nodejs.org/en/).
#### Tips: 运行之前请在根目录安装依赖库.
```
    npm install
```
### Quick Start
```
    npm start
```
在本地浏览器访问[https://localhost:8080](https://localhost:8080)<br>
![截图](./screenshot/20180730.PNG)<br>
[gen](./gen)文件夹下的证书是为本地dev生成,并不涉及任何机密信息.

# LICENSE
## [MIT](LICENSE)
