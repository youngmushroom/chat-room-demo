#!/bin/bash
gen () {
    rm -rf $1
    mkdir $1
    openssl req -newkey rsa:2048 -nodes -keyout $1/ca.key -x509 -days 365 -out $1/cert.crt -subj "/C=CN/ST=SX/L=XA/O=SSL/OU=com.gitlab/CN=gitlab.com"
}
opensslExist () {
    if command -v openssl >/dev/null 2>&1; then 
       GEN_DIR=./gen
       gen $GEN_DIR
       echo "gen ${GEN_DIR}/ca.crt using openssl"
    else
        echo 'please install OpenSSL.' 
    fi
}
opensslExist