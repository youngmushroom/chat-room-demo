import CustomMessgeChannel, { Message } from "./message";

interface MessageServer {
    accept(message: Message): void
}

class DefaultServer implements MessageServer {
    private _channels: CustomMessgeChannel[] = [];
    private _messageCache: Message[] = [];
    accept(message: Message): void {
        let messgaeSend = false;
        this._channels.forEach((channel: CustomMessgeChannel) => {
            if (message.to === channel.getGuid()) {
                messgaeSend = true;
                this.sendCachedMessge(channel);
                channel.onMessage(message);
            }
        });
        if (!messgaeSend) {
            this._messageCache.push(message);
        }
    }
    private sendCachedMessge(channel: CustomMessgeChannel): void {
        let index: number = 0,
            message: Message;
        while (message = this._messageCache[index]) {
            if (message.to === channel.getGuid()) {
                channel.onMessage(this._messageCache.splice(index, 1)[0]);
            } else {
                index++;
            }
        }
    }
    disconnect(channel: CustomMessgeChannel) {
        for (let index = 0; index < this._channels.length; index++) {
            const _channel = this._channels[index];
            if (channel === _channel) {
                this._channels.splice(index, 1);
                return;
            }
        }
    }

    connect(channel: CustomMessgeChannel): void {
        if (channel && this._channels.indexOf(channel) < 0) {
            this._channels.push(channel);
            this.sendCachedMessge(channel);
        }
    }

}
export var Server = new DefaultServer();