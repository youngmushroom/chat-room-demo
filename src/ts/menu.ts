import { i18n } from "./i18n/I18n";

export class Menu {
    private _onClose;
    private _onReset;
    private _host: HTMLElement;
    constructor(host: HTMLElement) {
        this._host = host;
        this.initI18n();
        this.attachEvent();
    }
    private attachEvent() {
        let close = this.getCloseButton();
        if (close) {
            close.addEventListener("click", (e: MouseEvent) => {
                if (this._onClose) {
                    this._onClose.call(this);
                }
            });
        }
        let set = this.getResetButton();
        if (set) {
            set.addEventListener("click", (e: MouseEvent) => {
                if (this._onReset) {
                    this._onReset.call(this);
                }
            });
        }
    }
    private initI18n(): void {
        this.getCloseButton().title = i18n.CloseButtonTitle;
        this.getResetButton().title = i18n.ResetButtonTitle;
    }
    private getCloseButton():HTMLElement {
        return this._host.querySelector(".chat-setting__close");
    }
    private getResetButton():HTMLElement {
        return this._host.querySelector(".chat-setting__reset");
    }
    onClose(callback) {
        this._onClose = callback;
    }
    onReset(callback) {
        this._onReset = callback;
    }
}