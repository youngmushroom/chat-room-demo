export var Util = {
    dateTimeFormat: function (time: number | Date) {
        let test = new Date();
        if ("number" === typeof time) {
            test.setTime(time);
        } else if (time instanceof Date) {
            test.setTime(time.getTime());
        }
        return Intl.DateTimeFormat(navigator.language, {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: false,
        }).format(test);
    }
}