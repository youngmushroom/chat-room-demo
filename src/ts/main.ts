import { i18n } from "./i18n/I18n";
import { User } from "./user";
import { Server } from "./message.server";
import { Util } from "./util/utils";
import CustomMessgeChannel, { Message } from "./message";
import { Menu } from "./menu";
export class App implements CustomMessgeChannel {
    private gui: ChatGui;
    private _user: User;
    private menu: Menu;
    private onLine: boolean = false;
    constructor(host: HTMLElement, userId: string, friendId: string) {
        this.user = new User(userId, friendId);
        this.connect();
        this.initGui(host);
        this.initMenu(host);
    }

    private initMenu(host: HTMLElement) {
        this.menu = new Menu(host);
        this.menu.onClose(() => {
            this.disconnect();
        });
        this.menu.onReset(() => {
            this.connect();
        });
    }


    private initGui(host) {
        this.gui = new ChatGui(host);
        this.gui.onSendMessage((message: string) => {
            this.sendMessage({
                message,
                time: Date.now(),
                from: this.user.id,
                to: this.user.friend,
            });
        });
    }

    /// message from server
    public onMessage(message: Message): void {
        this.gui.onMessage(message, false);
    }
    // mine 
    public sendMessage(message: Message): void {
        if (this.onLine) {
            Server.accept(message);
        }
        this.gui.onMessage(message, true);
    }
    private connect() {
        if (this.onLine) {
            return;
        }
        this.onLine = true;
        console.debug("connect...");
        Server.connect(this);
    }
    
    private disconnect(){
        if (!this.onLine) {
            return;
        }
        this.onLine = false;
        console.debug("disconnect...");
        Server.disconnect(this);
    }
    public getGuid() {
        return this.user.id;
    }
    public get user() {
        return this._user;
    }
    public set user(user: User) {
        this._user = user;
    }
}

class ChatGui {
    private _host: HTMLElement;
    private _onMessageCallback: (message: string) => void;
    constructor(host: HTMLElement) {
        this.host = host;
        this.init();
    }
    private init(): void {
        this.initGUI();
        this.initI18n();
        this.attachEvent();
    }
    
    onSendMessage(callback: (message: string) => void): void {
        this._onMessageCallback = callback;
    }
    private attachEvent() {
        let editor = this.getMessageEditor();
        let _gui = this;
        let send = (message: string) => {
            if (_gui._onMessageCallback) {
                _gui._onMessageCallback.call(this, message);
            }
        }

        editor.addEventListener("keydown", function (e: KeyboardEvent) {
            if (13 === e.keyCode) { // enter_key send message
                if (!this.value) {
                    return;
                }
                send(this.value);
                this.value = "";
                e.preventDefault();
                e.stopPropagation();
            }
        });

        let sendButton = this.getSendButton();
        sendButton.addEventListener("click", (e: MouseEvent) => {
            if (!editor.value) {
                return;
            }
            send(editor.value);
            editor.value = "";
            e.preventDefault();
            e.stopPropagation();
        });
    }

    public onMessage(message: Message, mine: boolean) {
        let temp: DocumentFragment = undefined;
        if (mine) {
            temp = this.createMessageTemplate({
                containerClass: "chat-reciver__container",
                class: "chat-reciver",
                time: Util.dateTimeFormat(message.time),
                message: message.message,
            });
        } else {
            temp = this.createMessageTemplate({
                containerClass: "chat-sender__container",
                class: "chat-sender",
                time: Util.dateTimeFormat(message.time),
                message: message.message,
            });
        }
        this.getMessageList().appendChild(temp);
        this.autoScrollIntoView();
    }
    private autoScrollIntoView(): void {
        // this.getLastMessgeLi().scrollIntoView(false);
    }


    private createMessageTemplate(option): DocumentFragment {
        let fragement = document.createDocumentFragment();
        let li = document.createElement("li");
        li.className = `chat-text ${option.containerClass}`;
        li.dataset["time"] = option.time;
        let message = document.createElement("div");
        message.className = option.class;
        message.textContent = option.message;
        li.appendChild(message);
        fragement.appendChild(li);
        return fragement;
    }

    private initGUI(): void {
        let temp = this.getGUITemplate();
        let fragement = document.createDocumentFragment();
        let container = document.createElement("div");
        container.innerHTML = temp;
        container.className = "chat-flex__center";
        fragement.appendChild(container);
        this.host.appendChild(fragement);
    }
    private getGUITemplate(): string {
        let temp = document.getElementById('chat-template');
        if (temp) {
            return temp.innerHTML;
        }
        return "";
    }
    private initI18n(): void {
        this.getSendButton().textContent = i18n.SenderButton;
        this.getSendButton().title = i18n.SenderButton;
    }

    private getLastMessgeLi(): HTMLLIElement {
        return this.getMessageList().lastElementChild as HTMLLIElement;
    }

    private getSendButton(): HTMLDivElement {
        return this.host.querySelector(".chat-send__button");
    }
    private getMessageEditor(): HTMLTextAreaElement {
        return this.host.querySelector(".chat-message-editor");
    }
    private getMessageList(): HTMLUListElement {
        return this.host.querySelector(".chat-list");
    }
    public set host(host: HTMLElement) {
        this._host = host;
    }
    public get host() {
        return this._host;
    }

}

new App(document.getElementById("chat1"), "123456", "654321");
new App(document.getElementById("chat2"), "654321", "123456");
