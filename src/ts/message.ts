
export interface Message {
    from: string;
    to: string;
    time: number,
    message: string;
}
export default interface CustomMessgeChannel {
    onMessage(messge: Message): void;
    getGuid(): string;
}