import I18n from "./I18n";
export class DefaultZHI18n implements I18n {
    CloseButtonTitle: string = "关闭";
    ResetButtonTitle: string = "重置";
    SenderButton: string  = "发送";
}