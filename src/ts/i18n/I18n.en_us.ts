import I18n from "./I18n";
export class DefaultEnI18n implements I18n {
    SenderButton: string = "Send";
    ResetButtonTitle: string = "Reset";
    CloseButtonTitle: string = "Close";
}