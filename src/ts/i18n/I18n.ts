import { DefaultZHI18n } from "./I18n.zh_cn";
import { DefaultEnI18n } from "./I18n.en_us";
export default interface I18n {
    SenderButton: string;
    CloseButtonTitle: string;
    ResetButtonTitle: string;
}
export var i18n: I18n = (() => {
    let language = navigator.language;
    if ("zh-cn" === language.toLowerCase()) {
        return new DefaultZHI18n();
    } else {
        return new DefaultEnI18n();
    }
})();
