export class User {
    private _id: string;
    private _name: string;
    private _friend: string;
    constructor(id: string, friend) {
        this.id = id;
        this._friend = friend;
    }

    public set name(name: string) {
        this._name = name;
    }
    public get name() {
        return this._name;
    }
    public set id(id: string) {
        this._id = id;
    }
    public get id(): string {
        return this._id;
    }
    set friend(friend) {
        this._friend = friend;
    }
    get friend() {
        return this._friend;
    }
}